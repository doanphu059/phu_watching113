<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('admins')->insert([
            'name'     => 'phupro',
            'email'    => 'admin113@gmail.com',
            'phone'    => '0708170704',
            'password' => Hash::make('Dieuchiaye113')
        ]);
    }
}
